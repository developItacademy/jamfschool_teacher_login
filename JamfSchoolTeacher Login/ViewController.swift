//
//  ViewController.swift
//  JamfSchoolTeacher Login
//
//  Created by Steven Hertz on 1/17/21.
//

import UIKit

extension String {
    func removingLeadingSpaces() -> String {
        guard let index = firstIndex(where: { !CharacterSet(charactersIn: String($0)).isSubset(of: .whitespaces) }) else {
            return self
        }
        return String(self[index...])
    }
}



class ViewController: UIViewController {


    var jamfSchoolApi: JamfSchoolAPI?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        jamfSchoolApi = JamfSchoolAPI(dataRequest: .getUser, endPoint: .getUser(userID: "367"))
        let myRC = MyRequestController()
        myRC.sendRequest(with: jamfSchoolApi?.generatedURL, andHeaderLines: jamfSchoolApi!.headerLinesToInsert, returnThis: UserDetailResponse.self) {(rslt) in
            switch rslt {
                case .success(let userDetailResponse): print(userDetailResponse.user.firstName)
                case .failure(let error): print(error)
            }
            print("finished")
        }
    }

 

}

