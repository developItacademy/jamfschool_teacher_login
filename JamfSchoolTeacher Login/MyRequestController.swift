//
//  MyRequestController.swift
//  JamfSchoolTeacher Login
//
//  Created by Steven Hertz on 1/18/21.
//

import Foundation
class MyRequestController {
    
    typealias CompletionT<T> = (Result<T,Error>) -> Void
    
    enum SendRequestError: Error {
        case http
    }
    
    func sendRequest<T: Codable>(with url: URL?,
                     andHeaderLines headerLines: [(k:String, v: String)?],
                     returnThis typeToReturn: T.Type,
                     whenFinishedDo completionHandler: @escaping CompletionT<T> ) {
        /* Configure session, choose between:
           * defaultSessionConfiguration
           * ephemeralSessionConfiguration
           * backgroundSessionConfigurationWithIdentifier:
         And set session-wide properties, such as: HTTPAdditionalHeaders,
         HTTPCookieAcceptPolicy, requestCachePolicy or timeoutIntervalForRequest.
         */
        
        
        func setUpRequest() -> (session: URLSession, request: URLRequest) {
            
            // Create session, and optionally set a URLSessionDelegate.
            let session: URLSession = {
                let sessionConfig = URLSessionConfiguration.default
                return URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            }()
            
            // making the urlRequest
            let request: URLRequest = {
                guard let url = url else {fatalError("could not convert the url")}
                print(url.absoluteString)
                var request = URLRequest(url: url)
                
                // method
                request.httpMethod = "GET"
                
                // Headers
                for headerLine in headerLines {
                    guard let hdrK = headerLine?.k, let  hdrV = headerLine?.v
                        else {fatalError("Could not load header lines")}
                    request.addValue(hdrV, forHTTPHeaderField: hdrK)
                }
                
                return request
            }()
            
            return (session: session, request: request)
        }

        
        func runTheRequest(_ reqComponents: (session: URLSession, request: URLRequest), _ completionHandler: @escaping CompletionT<T>) {
            //        request.addValue("Basic NTM3MjI0NjA6RVBUTlpaVEdYV1U1VEo0Vk5RUDMyWDVZSEpSVjYyMkU=", forHTTPHeaderField: "Authorization")
            //        request.addValue("2", forHTTPHeaderField: "X-Server-Protocol-Version")
            //        request.addValue("hash=5b011ad78fdad1fefb78b99b7493f96a", forHTTPHeaderField: "Cookie")
            
            /* Start a new Task */
            let task = reqComponents.session.dataTask(with: reqComponents.request) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
                
                guard error == nil else {
                    let resultError: Result<T,Error> = .failure(error!)
                    completionHandler(resultError)
                    return
                }
                guard (response as! HTTPURLResponse).statusCode == 200 else {
                    let resultError: Result<T,Error> = .failure(SendRequestError.http)
                    completionHandler(resultError)
                    return
                }
                guard let data = data else { fatalError("Data was nil")}
                
                let statusCode = (response as! HTTPURLResponse).statusCode
                print("URL Session Task Succeeded: HTTP \(statusCode)")
                                
                let decoder = JSONDecoder()
                
                guard let userDetailResponse = try? decoder.decode(typeToReturn, from: data) else { fatalError("error decoding it.")}

                let m: Result<T,Error> = .success(userDetailResponse)
                
                completionHandler(m)
            }
            
            task.resume()
            reqComponents.session.finishTasksAndInvalidate()
        }
        //  MARK: -  Do the setup of the request qnd the execute of the request
        
        let reqComponents = setUpRequest()

        runTheRequest(reqComponents, completionHandler)

    }
    
    
}

