//
//  UserDetailResponse.swift
//  JamfSchoolTeacher Login
//
//  Created by Steven Hertz on 2/5/21.
//

import Foundation
struct UserDetailResponse: Codable {
    static func doConvert() {
        print("do convert")
    }
    
    let code: Int
    let user: User
    
    static func loadTheData() -> UserDetailResponse {
        guard   let url = Bundle.main.url(forResource: "deviceGroups", withExtension: "json"),
                let jsonString = try?  String(contentsOf: url),
                let jsonData = jsonString.data(using: .utf8)
        else { fatalError("could not get file") }
        
        guard let json = try? JSONDecoder().decode(UserDetailResponse.self, from: jsonData) else {
            fatalError("decoding")
        }
        
        
        return json
        // print(jsonString)
    }
    
}
