//
//  JamfSchoolAPI.swift
//  JamfSchoolTeacher Login
//
//  Created by Steven Hertz on 1/17/21.
//

import Foundation

enum EndPoint {
    case validateTeacher
    case getUser(userID: String)
    case getListOfClasses
    case getAClass
}

enum DataRequest: String {
    case validateTeacher
    case getUser = "/users"
    case getListOfClasses
    case getAClass
}

/*
 validateTeacher    -> User Id of Teacher
 getUser(userId)    -> UserDeail with teachers group
 getListOfClasses   -> ClassList with group Number and class identifier for each class
 getAClass(classId) -> Class detail including students and pictures
 */

enum PossibleErrors: Error {
    case urlCreate
}

struct JamfSchoolAPI {
    
    typealias headerValuesType = (optKey:String , headerData:(key:String, value: String) )
    typealias headerDictType = Dictionary<String,(k:String, v: String)>

    //  MARK: -  Type Properties
    private static let baseUrl = "https://api.zuludesk.com"
    private static let apiKey = "ddkjd"
    
    
    //  MARK: -  Instance Properties

    let dataRequestHeaders: Dictionary<DataRequest,Array<String>> = [
        .getUser:["1","2","3"],
        .getAClass:["1","3"]
    ]

    var dataRequest: DataRequest
    var endPoint: EndPoint
    
    var headerDict: headerDictType = [:]
    
    var actionListForRequest: Array<String> {
        return dataRequestHeaders[dataRequest]!
    }
    
    var headerLinesToInsert: [(k:String, v: String)?] {
        let x = actionListForRequest.map { headerDict[$0] }
        return x
    }
    
    var generatedURL: URL {
        generateURL()
    }

    func logMessage(_ message: String,
                    fileName: String = #file,
                    functionName: String = #function,
                    lineNumber: Int = #line,
                    columnNumber: Int = #column) {
        
//        print("🤡🤡🤡 Called by \(fileName) - \(functionName) at line \(lineNumber)[\(columnNumber)]")
    }
    
    func generateURL() -> URL {
        
        guard var urlComponents = URLComponents(string: JamfSchoolAPI.baseUrl) else {fatalError("failed initializing urlComponents")}
        urlComponents.path = self.dataRequest.rawValue
        switch endPoint {
        case .getUser(userID: let userID):
            urlComponents.path += "/" + userID
        default:
            print("in default")
        }

        // guard let url = urlComponents.url else { throw PossibleErrors.urlCreate}
        return urlComponents.url!
        
//           components.queryItems = [
//               URLQueryItem(name: "q", value: query),
//               URLQueryItem(name: "sort", value: sorting.rawValue)
//           ]

   }

    
    //  MARK: -  Init
    init(dataRequest: DataRequest, endPoint: EndPoint) {
        
        guard let filePath = Bundle.main.path(forResource: "example", ofType: "txt"), let dataString = try? String(contentsOfFile: filePath) else {
            fatalError("Could not het the filePath")
        }
        
        // instantiate the dictionary
        var dict: headerDictType = [:]
        
        
        dataString.enumerateLines {  (line, _) in
            // guard let self = self else {return}
            // let ret = self.processLineHeaderValueConfigurationFile(with: line)
            let inputSplit = line.split(whereSeparator: { $0 == "," }).map {String($0).removingLeadingSpaces() }
            let ret: headerValuesType = ( optKey:inputSplit[0], ( key:inputSplit[1], value:inputSplit[2]) )
            dict.updateValue((k: ret.headerData.key, v: ret.headerData.value), forKey: ret.optKey)
        }
        
        self.headerDict = dict
        
        self.dataRequest = dataRequest
        self.endPoint = endPoint
        
    }
    
    
}



